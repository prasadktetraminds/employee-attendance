package com.tminds;

public class AppResponse {

    //private String id;
    private Object result;
    private String Status;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }


    public Object getResult()
    {
        return result;
    }

    public void setResult(Object result)
    {
        this.result=result;
    }


}
