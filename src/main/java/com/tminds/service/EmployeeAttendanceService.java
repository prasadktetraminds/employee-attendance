package com.tminds.service;

import com.tminds.entities.EmployeeAttendance;
import com.tminds.repository.EmployeeAttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;


@Service
public class EmployeeAttendanceService {
    //this reffernce is used to use the methods which are used in repository
    private final EmployeeAttendanceRepository repository;

    @Autowired
    EmployeeAttendanceService(EmployeeAttendanceRepository repository){
        this.repository=repository;
    }

    //this method used to insert the employee work status
    public EmployeeAttendance create(EmployeeAttendance ea) {

        long day = new Date().getTime();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = df1.format(day).toString();
        ea.setDay(dateToStr);
        //Date startTime= startTime;
       // DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm");
        //dateToStr = df.format(startTime).toString();
        //ea.setStartTime(startTime.toString());
        repository.save(ea);
        return ea;
    }

    //this method used to find all the employees data available in database
    public List<EmployeeAttendance> findAll() {
        List<EmployeeAttendance> todoEntries=repository.findAll(new Sort(Sort.Direction.DESC, Arrays.asList("_id")));
        return todoEntries;
    }

    public EmployeeAttendance delete(String id) {
        EmployeeAttendance deleted = findById(id);
        repository.delete(deleted);
        return deleted;
    }

    //this method is used to find indidual employee status using id
    public EmployeeAttendance findById(String id) {
        EmployeeAttendance found=findeaById(id);
        return found;
    }

    public EmployeeAttendance findeaById(String id) {
        Optional<EmployeeAttendance> result=repository.findOne(id);
        return result.get();
    }

    //this method is used to find the employee total work status on end of the day
    public EmployeeAttendance update(EmployeeAttendance ea) throws ParseException {
         EmployeeAttendance updated=findeaById(ea.getId());
         //calculate duration
        /*1. convert Endtime string to date object
          2. convert StartTime String to date object
          3. Calulate difference between both date objects as duration and save
         */
        DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm");

        //1. convert endtime to date object
        Date endDate = df.parse(ea.getEndTime());
        updated.setEndTime(ea.getEndTime());

        //2. convert startDtm to date object
        Date startDate = df.parse(updated.getStartTime());
        updated.setStartTime(updated.getStartTime());

        //3. calculation
        long duration  = endDate.getTime() - startDate.getTime();
        long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
        long diffInMIns = TimeUnit.MILLISECONDS.toMinutes(duration);
        String durationStr = diffInHours+"h:"+(diffInMIns - diffInHours*60);

        updated.setDuration(durationStr);
        updated=repository.save(updated);

        return updated;

    }

     //this method used to find the employee data day by day using empid
    public List<EmployeeAttendance> findEmployeeAttandanceByQuery(String empId,String day) {
        List<EmployeeAttendance> res2=repository.findEmployeeAttandanceByQuery(empId,day);
        return res2;
    }

    //this method is used for adding empid
    public EmployeeAttendance getByEmpId(String empId) {
        EmployeeAttendance res=repository.findEmpById(empId);
        return res;
    }

}
