package com.tminds.service;

import com.tminds.entities.Employee;
import com.tminds.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService
{
    @Autowired
    //this is the refference taken from Repository for calling the methods of repository
    private EmployeeRepository repository;


//this method used to creating an employee and save the employee result
    public Employee createEmployee(Employee employee){
        repository.save(employee);
        return employee;
    }
    //this method is used to validating the employee data is existing in database or not
    public Employee validate(Employee employee){
       List<Employee> exisistingEmps= repository.findEmpByUserName(employee.getUserName());
       for (Employee emp:exisistingEmps) {
           if(emp.getPassword().equals(employee.getPassword())){
               return  emp;
           }

       }
       return null;
    }

    //this method gets the indidual employee data and stored in a variable and return that value
    public Employee findById(String id) {
        Optional<Employee> result=repository.findOne(id);
        return result.get();
    }
    // This method used to get all the employees data from  database repository
    public List<Employee> findAll()
    {
        List<Employee> empEntries=repository.findAll();
        return empEntries;
    }
    public List<Employee> findAllEmployee()
    {
        List<Employee> empEntries=repository.findAll();
        return empEntries;
    }
    //This method is used to updating the employee data exisiting in database repository
    public Employee update(Employee emp) {
        Employee updated=findById(emp.getId());
        updated.setUserName(emp.getUserName());
        updated=repository.save(updated);
        return updated;

    }


}
