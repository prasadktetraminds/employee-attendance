package com.tminds.controller;

import com.tminds.AppResponse;
import com.tminds.entities.Employee;
import com.tminds.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/emp")
public class EmployeeController {

    @Autowired
    private  EmployeeService employService;

    //this method call post method for creating new employee data in database
    @RequestMapping(method= RequestMethod.POST)
    public AppResponse createEmployee(@RequestBody Employee employee)
    {
        AppResponse appResponse =new AppResponse();

        //the try and catch blocks are used to handel exceptions occured during run time
        try {
            employService.createEmployee(employee);
            Map<String,String> resultMap = new HashMap();
            resultMap.put("id",employee.getId());
            resultMap.put("name",employee.getName());
            resultMap.put("username",employee.getUserName());
            resultMap.put("message","Successfully created new Employee");

            appResponse.setResult(resultMap);
            appResponse.setStatus("Success");

        }catch (Exception e){
            e.printStackTrace();
            appResponse.setStatus("Failed to create");
            appResponse.setResult("this record already exist ");
        }
        return appResponse;
    }

    // this method also called post method to check login credentials of an employee
    @RequestMapping(value="/login",method=RequestMethod.POST)
    public AppResponse validate (@RequestBody Employee employee)
    {
        AppResponse appResponse = new AppResponse();
        try {
            Employee emp = employService.validate(employee);
            if (emp == null) {
                appResponse.setStatus("TryAgain");
                appResponse.setResult("Invalid Credentials");
                return appResponse;
            }


            Map<String, String> resultMap = new HashMap();
            resultMap.put("id", emp.getId());
            resultMap.put("name",emp.getName());
            resultMap.put("username", employee.getUserName());
            resultMap.put("message", "Welcome to Tetraminds");

            appResponse.setResult(resultMap);
            appResponse.setStatus("Success");

            return appResponse;


        } catch (Exception e) {
            e.printStackTrace();

            appResponse.setStatus("Try Again");
            appResponse.setResult("Invalid Credentials");
            return appResponse;

        }
    }


   //this method calls GET to find the employee data from database
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public AppResponse findOne(@PathVariable("id") String id) {
        AppResponse appResponse = new AppResponse();

        try {
            Employee emp1 = employService.findById(id);
            if (emp1 == null) {
                appResponse.setStatus("failure");
                appResponse.setResult("no data with this id");
                return appResponse;
            }
                Map<String, String> resMap1 = new HashMap();
                resMap1.put("id", emp1.getId());
                resMap1.put("name",emp1.getName());
                resMap1.put("userName", emp1.getUserName());
                resMap1.put("message", "Welcome to Tetraminds");

                appResponse.setResult(resMap1);
                appResponse.setStatus("Success");
                return appResponse;


        } catch (Exception e) {
            e.printStackTrace();
            appResponse.setStatus("Failure");
            appResponse.setResult("No Data Found For Such id");
            return appResponse;

        }
    }

     //This method also call GET to find all employees data available in database
    @RequestMapping(method=RequestMethod.GET)
    public List<Employee> findAll(){


        return  employService.findAll();
    }
    @RequestMapping(value = "/all",method=RequestMethod.GET)
    public List<Employee> getAllEmployess(){
        return employService.findAllEmployee();
    }

     //this method calls PUT to update the employee data of database repository
    @RequestMapping(value="/upd",method=RequestMethod.PUT)
     Employee update(@RequestBody @Valid Employee todoEntry) {

        return employService.update(todoEntry);

    }




}
