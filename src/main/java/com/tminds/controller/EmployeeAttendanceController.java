package com.tminds.controller;

import com.tminds.entities.EmployeeAttendance;
import com.tminds.service.EmployeeAttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/*

 */
@RestController
@RequestMapping("/attendence")
public class EmployeeAttendanceController {

    @Autowired
    EmployeeAttendanceService employeeAttendanceService;
/*

 */
     // This method calls POSt to start the timer and shows the employee work status
    @RequestMapping(value = "/start",method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    EmployeeAttendance create(@RequestBody EmployeeAttendance ea) {
        return employeeAttendanceService.create(ea);
    }
/*
This method calls GET to find all the employee data avilable in database
 */
    @RequestMapping(method = RequestMethod.GET)
    List<EmployeeAttendance> findAll() {
        try {
            return employeeAttendanceService.findAll();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
/*

in this method calls delete for deleting emplyee records
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    EmployeeAttendance delete(@PathVariable("id") String id){
        return employeeAttendanceService.delete(id);
    }
/*
in this method find employee data
 */
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    EmployeeAttendance findById(@PathVariable("id") String id){
        return employeeAttendanceService.findById(id);
    }
    /*
    in this method calls POST to stop the timer and gives work duration of employee
     */
    @RequestMapping(value = "/stop",method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    EmployeeAttendance update(@RequestBody EmployeeAttendance ea)throws Exception {
        try {
            return employeeAttendanceService.update(ea);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    /*
     this method calls GET to find indidual employee data with date
     */

     @RequestMapping(value = "/emp/{empId}/attendence",method = RequestMethod.GET)
     List<EmployeeAttendance> findEmployeeAttandanceByQuery(@PathVariable String empId,@RequestParam String day){
         return employeeAttendanceService.findEmployeeAttandanceByQuery(empId,day);
     }
     /*
      this method calls POSt to adding employee id
      */
    @RequestMapping(value = "/emp/{empId}/stop",method = RequestMethod.POST)
    EmployeeAttendance getByEmpId(@RequestBody String empId){
        return employeeAttendanceService.getByEmpId(empId);

    }

     //for handling exceptions
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleTodoNotfound(Exception ex){

    }

}
