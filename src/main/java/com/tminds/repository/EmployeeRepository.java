package com.tminds.repository;

import com.tminds.entities.Employee;
import com.tminds.entities.EmployeeAttendance;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends Repository<Employee, String>
{
    //This method is used to save the employee details in repository
    Employee save(Employee employee);

    //@Query(value="{}", fields="{ 'password' : 0}")
    List<Employee> findAll();

    // This is query is used to find the particular employee data with user name
    @Query("{'userName': ?0}")
    List<Employee> findEmpByUserName(String userName);

    Optional<Employee> findOne(String id);

    // this Method is used to delete the particular record from database repository
    Employee delete(Employee deleted);


    Employee findById(String id);


}
