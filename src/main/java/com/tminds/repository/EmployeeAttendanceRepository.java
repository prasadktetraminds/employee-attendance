package com.tminds.repository;

import com.tminds.entities.EmployeeAttendance;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface EmployeeAttendanceRepository extends Repository<EmployeeAttendance,String> {
    //this method used to save the employee data in repository
    EmployeeAttendance save(EmployeeAttendance saved);

    //@Query("{ $query: {}, $orderby: { startTime: -1 } }")
    List<EmployeeAttendance> findAll(Sort sort);
  //this query used to filter the status day by day

    @Query("{$and:[{'empId': ?0},{'day': ?1}]}")
    List<EmployeeAttendance> findEmployeeAttandanceByQuery(String empId,String day);

    Optional<EmployeeAttendance> findOne(String id);

    void delete(EmployeeAttendance deleted);

    //this query is used to adding empId
    @Query("{'empId': ?0}")
    List<EmployeeAttendance> getByEmpId(String empId);

    EmployeeAttendance findEmpById(String empId);




}
